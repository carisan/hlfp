Datos Históricos de la Liga de Fútbol Profesional española.
===========================================================

Base de datos en formato JSON con los resultados históricos, desde 1972, de la Liga de Fútbol Profesional de España.

Meta
----

* Autor: Carlos Arija
* Contacto: carlos.arija@outlook.com
* Actualizado: 2019-09-2 09:25:00 
     * División 1: jornada 3
     * División 2: jornada 3

Proyecto
--------

Esta base de datos se ha realizado con los datos obtenidos de la página http://liga.host56.com/ (abandonado desde la liga 2016), añadiendo los datos que aparecen en la página de la liga de fútbol profesional https://www.laliga.es/ 

Dado que HOST56 tenía datos desde 1972, pero LFP solo tiene datos desde 1982, combinar los datos de ambas fuentes ha implicado:

* La conversión del formato LIGA.HOST56 a JSON
* La conversión de tablas web de LFP a JSON
* Corrección de datos de la web de la LFP
* Conversión de nombres de equipos de LIGA.HOST56 a los equivalentes en LFP

Objetivo
--------

Ofrecer una forma simple y actualizada de acceder a los resultados históricos de los partidos de la liga de fútbol española. 

Formato
-------

En la elección entre simplicidad y tamaño, se ha elegido la simplicidad.

Por eso en lugar de ofrecer varias tablas relacionales o una jerarquía de objetos JSON, que hubieran reducido el tamaño de los ficheros, se ha preferido crear una única tabla monolítica en la que cada elemento contiene todos los datos de un partido.

Aun así, se ha intentado reducir el tamaño del fichero final limitando los nombres de los campos de datos a 1 o 2 caracteres y eliminando todos los espacios de separación no necesarios.

El formato es el siquiente:

```
[{"D":1,"L":1972,"J":1,"EL":"Granada CF","EV":"R. Zaragoza","GL":0,"GV":0},
{"D":1,"L":1972,"J":1,"EL":"FC Barcelona","EV":"RC Deportivo","GL":3,"GV":1},
{"D":1,"L":1972,"J":1,"EL":"Atlético de Madrid","EV":"Valencia CF","GL":1,"GV":3},
{"D":1,"L":1972,"J":1,"EL":"UD Las Palmas","EV":"R. Oviedo","GL":2,"GV":1},
{"D":1,"L":1972,"J":1,"EL":"Real Sporting","EV":"Real Betis","GL":1,"GV":0},
....
{"D":2,"L":2018,"J":17,"EL":"Elche C.F.","EV":"Real Sporting","GL":0,"GV":0},
{"D":2,"L":2018,"J":17,"EL":"CD Lugo","EV":"UD Las Palmas","GL":4,"GV":2},
{"D":2,"L":2018,"J":17,"EL":"R. Zaragoza","EV":"Córdoba CF","GL":0,"GV":0}]]
```

Cada objeto representa un partido en el que se incluye:

* "D": División 
* "L": Año de la Liga
* "J": Jornada
* "EL": Equipo Local
* "EV": Equipo Visitante
* "GL": Goles del equipo Local
* "GV": Goles del equipo Visitante

Inconsistencias a tener en cuenta
---------------------------------

* No se garantiza un orden específico de los partidos en el archivo.
    * Los partidos, aunque normalmente están agrupados por división, temporada y jornada, no tienen por qué seguir ningún orden específico.
* El número de la jornada no indica un orden temporal.
    * Ejemplo: Un partido de la jornada 1 que se aplace, puede jugarse después de la jornada 4.
* No se garantiza un número constante de equipos o jornadas en las ligas.   
    * El número de equipos y de jornadas de cada liga ha variado a lo largo de la historia. Ejemplo: 1995/1996 - liga de 22 equipos.
* No se garantiza que los resultados sean consecuencia real de un partido.
    * Ejemplo: La expulsión de un equipo (Reus 2018) según la normativa de la liga, hace que pierda todos sus partidos por un 1 a 0. Esto da lugar a resultados ficticios.

Encoding
--------

El fichero tiene formato UTF-8 con finales de línea tipo Windows(CR LF).

Nombres de los equipos
----------------------

Para los equipos se utilizan las designaciones de la página de la LFP, conservando los caracteres con acentos, diéresis y demás símbolos ortográficos españoles.

Estos nombres pueden no coincidir (y de hecho muchos no coinciden ¯\\\_(ツ)_/¯ ) con los utilizados en "La Quiniela" de Loterías y Apuestas del Estado. 
